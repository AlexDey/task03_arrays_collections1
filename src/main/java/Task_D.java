import java.util.concurrent.ThreadLocalRandom;

class Task_D {
    static Hero hero = new Hero();
    private BehindTheDoor[] doors = new BehindTheDoor[10];
    /**
     * method starts the game
     */
    void getResults() {
        initDoors();
        getDeathDoors();
        getAliveDoorsOrder();
        System.out.println("Choose the door and enter the room!");
        for (int i = 0; i < doors.length; i++) {
            System.out.print("Door#" + (i + 1) + ": " +
                    doors[i].getClass().getSimpleName() + ": " +
                    doors[i].getPower() + "\n");
        }
        for (int i = 0; i < doors.length; i++) {
            doors[i].affectHero();
            if (hero.power <= 0) {
                System.out.println("Game over! Hero was defeated by the monster " +
                        "behind the door number " + (i + 1));
                break;
            }
        }
        if (hero.power > 0) {
            System.out.println("Congratulations! Hero is still alive. " +
                    "Hero's power is: " + hero.power + "\n");
        }
    }
    /**
     * method initializes doors
     */
    private void initDoors() {
        for (int i = 0; i < doors.length; i++) {
            doors[i] = generateRandom(0, 10) % 2 == 0 ?
                    new Artefact(generateRandom(10, 80)) :
                    new Monster(generateRandom(5, 100));
        }
    }

    /**
     * method prints out info about unsuccessful game's end
     */
    private void getDeathDoors() {
        StringBuilder sb = new StringBuilder("The death is waiting for " +
                "hero behind the doors: ");
        for (int i = 0; i < doors.length; i++) {
            if (doors[i] instanceof Monster && doors[i].getPower() > hero.power) {
                sb.append((i + 1)).append(" ");
            }
        }
        System.out.println(sb.toString());
    }

    /**
     * method prints out info about successful game's end
     */
    private void getAliveDoorsOrder() {
        System.out.println("If you want to stay alive, you should enter " +
                "the doors in that order: ");
        for (int i = 0; i < doors.length; i++) {
            if (doors[i] instanceof Artefact) {
                System.out.println("Door#" + (i + 1));
            }
        }
        for (int i = 0; i < doors.length; i++) {
            if (doors[i] instanceof Monster) {
                System.out.println("Door#" + (i + 1));
            }
        }
    }

    /**
     * method generates random ints
     *
     * @param min min limit
     * @param max max limit
     * @return random int of some limit
     */
    private int generateRandom(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }
}

abstract class BehindTheDoor {
    abstract int getPower();

    abstract void affectHero();
}

class Hero {
    int power = 25;
}

class Artefact extends BehindTheDoor {

    private int power;

    Artefact(int power) {
        this.power = power;
    }

    public int getPower() {
        return power;
    }

    @Override
    void affectHero() {
        Task_D.hero.power += this.power;
    }
}

class Monster extends BehindTheDoor {
    private int power;

    Monster(int power) {
        this.power = power;
    }

    public int getPower() {
        return power;
    }

    @Override
    void affectHero() {
        if (Task_D.hero.power < this.power) {
            Task_D.hero.power -= this.power;
        }
    }
}
