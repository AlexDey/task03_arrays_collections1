import java.util.Scanner;

/*
 * Copyright (c) Deyneka Oleksandr
 *
 */
public class Application {
    private static int[] array1 = new int[]{1, 9, 4, 3, 0, 1, 1, 5, 4, 9, 4, 4, 3, 9};
    private static int[] array2 = new int[]{5, 2, 6, 0, 4, 6, 5, 2, 8, 5, 2, 0};
    private static int[] array3 = new int[]{5, 5, 6, 1, 1, 1, 5, 2, 8, 2, 2, 2};

    /**
     * program's entry point
     *
     * @param args main args
     */
    public static void main(String[] args) {
        do {
            System.out.println("1 Task A \n" +
                    "2 Task B \n" +
                    "3 Task C \n" +
                    "4 Task D \n" +
                    "5 Task Generics \n" +
                    "0 Exit \n");
            Scanner sc = new Scanner(System.in);
            switch (sc.nextInt()) {
                case 1:
                    getTaskAResults();
                    break;
                case 2:
                    getTaskBResults();
                    break;
                case 3:
                    getTaskCResults();
                    break;
                case 4:
                    getTaskDResults();
                    break;
                case 5:
                    getTaskGenericsResults();
                    break;
                default:
                    System.exit(0);
            }
        } while (true);
    }

    /**
     * method prints out Task_A results
     */
    private static void getTaskAResults() {
        System.out.print("Array of common elements: ");
        printArray(new Task_A().findCommonInsideArrays(array1, array2));
        System.out.print("Array of non-common elements: ");
        printArray(new Task_A().findDifferInsideArrays(array1, array2));
    }

    /**
     * method prints out Task_B results
     */
    private static void getTaskBResults() {
        System.out.print("Array of non-triple elements: ");
        printArray(new Task_B().deleteTripleElements(array1));
    }

    /**
     * method prints out Task_C results
     */
    private static void getTaskCResults() {
        System.out.print("Array without same in-row elements: ");
        printArray(new Task_C().removeSameElements(array3));
    }

    /**
     * method prints out Task_D results
     */
    private static void getTaskDResults() {
        System.out.println("Task_D");
        new Task_D().getResults();
    }

    private static void getTaskGenericsResults() {
        Task_Generics task = new Task_Generics();
        task.doSomething();
        task.putStringIntoIntegerList();
    }

    /**
     * method prints out given array of ints
     */
    private static void printArray(int[] array) {
        for (int i : array) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

}
