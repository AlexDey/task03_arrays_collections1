import java.util.Random;

class Task_C extends AbstractTask {
    /**
     * method deletes same elements from the array
     *
     * @param array an array of ints
     * @return int array
     */
    int[] removeSameElements(int[] array) {
        int length = array.length;
        int max = 0;
        int temp = 0;
        for (int i : array) {
            if (i > max) {
                max = i;
            }
        }
        temp = ++max;
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] == array[i + 1]) {
                array[i] = temp;
                length--;
            }
        }
        int[] result = new int[length];
        int count = 0;
        for (int i : array) {
            if (i != temp) {
                result[count] = i;
                count++;
            }
        }
        return result;
    }
}
