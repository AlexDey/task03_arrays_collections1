abstract class AbstractTask {
    /**
     * Method removes double elements and converts String array to int array
     *
     * @param array an array of Strings
     * @return an array of ints
     */
    int[] convertIntoIntArray(String[] array) {
        for (int j = 0; j < array.length; j++) {
            for (int i = 0; i < array.length; i++) {
                if (j != i && array[j].equals(array[i])) {
                    array[j] = "del";
                }
            }
        }
        int length = 0;
        for (String s : array) {
            if (!s.equals("del")) {
                length++;
            }
        }
        int[] result = new int[length];
        int i = 0;
        for (String s : array) {
            if (!s.equals("del")) {
                result[i] = Integer.parseInt(s);
                i++;
            }
        }
        return result;
    }
}
