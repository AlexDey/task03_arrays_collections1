import java.util.Arrays;

class Task_A extends AbstractTask{
    /**
     * method finds common elements in both arrays
     *
     * @param array1 first array
     * @param array2 second array
     * @return int[] array
     */
    int[] findCommonInsideArrays(int[] array1, int[] array2) {
        StringBuilder sb = new StringBuilder();
        Arrays.sort(array1);
        for (int x : array2) {
            if (Arrays.binarySearch(array1, x) >= 0) {
                sb.append(x).append(" ");
            }
        }
        String[] temp = sb.toString().split(" ");

        return convertIntoIntArray(temp);
    }

    /**
     * method finds different elements in both arrays
     *
     * @param array1 first array
     * @param array2 second array
     * @return int[] array
     */
    int[] findDifferInsideArrays(int[] array1, int[] array2) {
        StringBuilder sb = new StringBuilder();
        Arrays.sort(array1);
        Arrays.sort(array2);
        for (int x : array2) {
            if (Arrays.binarySearch(array1, x) < 0) {
                sb.append(x).append(" ");
            }
        }
        for (int x : array1) {
            if (Arrays.binarySearch(array2, x) < 0) {
                sb.append(x).append(" ");
            }
        }
        String[] temp = sb.toString().split(" ");
        return convertIntoIntArray(temp);
    }
}
