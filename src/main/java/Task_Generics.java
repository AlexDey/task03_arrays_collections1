import java.util.ArrayList;
import java.util.List;

class Task_Generics {

    void doSomething() {
        DroidCarrier<SomeDroid> carrier = new DroidCarrier<>();
        carrier.loadDroid(new SomeDroid());
        SomeDroid tempDroid = carrier.unloadDroid();
    }

    void putStringIntoIntegerList() {
        List list = new ArrayList();
        List<Integer> listInt = new ArrayList<>();
        list = listInt;
        list.add("SomeString");
        System.out.println(list);
    }
}

class DroidCarrier<T> {
    private T droid;

    void loadDroid(T droid) {
        this.droid = droid;
    }

    T unloadDroid() {
        return this.droid;
    }
}

class SomeDroid {

}


