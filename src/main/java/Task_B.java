class Task_B extends AbstractTask {
    /**
     * method deletes all triple elements from the array
     *
     * @param array an array
     * @return int[] array
     */
    int[] deleteTripleElements(int[] array) {
        StringBuilder sb = new StringBuilder();
        int count = 0;
        for (int i : array) {
            for (int a : array) {
                if (i == a) {
                    count++;
                }
            }
            if (count < 3) {
                sb.append(i).append(" ");
            }
            count = 0;
        }
        String[] temp = sb.toString().split(" ");
        return convertIntoIntArray(temp);
    }
}
